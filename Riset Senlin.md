```
Environment :
OS : Ubuntu 18.04
CPU : 8
RAM : 23 GB
Disk : 100 GB

Openstack Version : Ussuri
```

# CHECK PROFILE TYPES
> ubuntu@riset-btech:~$ openstack cluster profile type list
```
+----------------------------+---------+----------------------------+
| name                       | version | support_status             |
+----------------------------+---------+----------------------------+
| container.dockerinc.docker | 1.0     | EXPERIMENTAL since 2017.02 |
| os.heat.stack              | 1.0     | SUPPORTED since 2016.04    |
| os.nova.server             | 1.0     | SUPPORTED since 2016.04    |
+----------------------------+---------+----------------------------+
```

# MEMBUAT CLUSTER 

1. Membuat file profile
> vim cirros_profile.yml

```
type: os.nova.server
version: 1.0
properties:
  name: cirros_server
  flavor: minimum
  image: "cirros"
  key_name: key
  networks:
   - network: internal
  metadata:
    test_key: test_value
  user_data: |
    #!/bin/sh
    echo 'hello, world' > /tmp/test_file
```

2. Membuat profile
> openstack cluster profile create --spec-file cirros_profile.yml cirros_profile

> openstack cluster profile list


3. Membuat cluster
> openstack cluster create --profile cirros_profile --desired-capacity 1 mycluster

> openstack cluster show mycluster


4. Cek node
> openstack cluster members list mycluster


``

# MANUAL SCALING CLUSTER

***## scale out ##***

1. Scale out 1 node
> openstack cluster expand mycluster

2. Cek detail cluster
> openstack cluster show mycluster

3. Scale out menjadi beberapa node
> openstack cluster expand --count 2 mycluster

4. Cek detail cluster
> openstack cluster show mycluster


***## scale in ##***

1. Scale in 1 node
> openstack cluster shrink mycluster

2. Cek detail cluster
> openstack cluster show mycluster

3. Scale in menjadi beberapa node 
> openstack cluster shrink --count 2 mycluster

4. Cek detail cluster
> openstack cluster show mycluster


***## Resize jumlah node cluster ##***


1. Resize dengan jumlah node tertentu
> openstack cluster resize --capacity 3 mycluster

2. Cek detail cluster
> openstack cluster show mycluster


# AUTO SCALING CLUSTER

1. Membuat Receiver dengan tipe webhook
> openstack cluster receiver create --type webhook --cluster mycluster --action CLUSTER_SCALE_OUT so_receiver

> openstack cluster receiver create --type webhook --cluster mycluster --action CLUSTER_SCALE_IN si_receiver

2. Buat scaling policy Scale Out

**Capacity**
> vim so_policy_capacity.yml
```
type: senlin.policy.scaling
version: 1.0
properties:
  event: CLUSTER_SCALE_OUT
  adjustment:
    type: CHANGE_IN_CAPACITY
    number: 1
    min_step: 2
    best_effort: True
    cooldown: 60
```

**Percentage**
> vim so_policy_percentage.yml
```
type: senlin.policy.scaling
version: 1.0
properties:
  event: CLUSTER_SCALE_OUT
  adjustment:
    type: CHANGE_IN_PERCENTAGE
    number: 50
    min_step: 2
    best_effort: True
    cooldown: 60
```

3. Buat scaling policy Scale In

**Capacity**
> vim si_policy_capacity.yml
```
type: senlin.policy.scaling
version: 1.0
properties:
  event: CLUSTER_SCALE_IN
  adjustment:
    type: CHANGE_IN_CAPACITY
    number: 1
    min_step: 2
    best_effort: True
    cooldown: 60
```

**Percentage**
> vim si_policy_percentage.yml
```
type: senlin.policy.scaling
version: 1.0
properties:
  event: CLUSTER_SCALE_IN
  adjustment:
    type: CHANGE_IN_PERCENTAGE
    number: 50
    min_step: 2
    best_effort: True
    cooldown: 60
```

4. Create policy dan attach ke cluster
> openstack cluster policy create --spec-file so_policy_capacity.yml so_policy_capacity

> openstack cluster policy create --spec-file si_policy_capacity.yml si_policy_capacity

> openstack cluster policy attach --policy so_policy_capacity mycluster

> openstack cluster policy attach --policy si_policy_capacity mycluster

> openstack cluster policy binding list mycluster

5. Cek receiver dan show
> openstack cluster receiver list

> openstack cluster receiver show *receiver_id*

6. Nodes yang terdapat pada cluster sebelum scaling
> openstack cluster members list mycluster

7. Akses webhook receiver
> curl -X POST *alarm_url*

8. Kemudian cek nodes dalam cluster  
> openstack cluster members list mycluster


# AUTO HEALING NODE FAILURES

1. Membuat health policy
> vim health_policy.yml

```
# Sample health policy based on node health checking
type: senlin.policy.health
version: 1.1
description: A policy for maintaining node health from a cluster.
properties:
  detection:
    # Number of seconds between two adjacent checking
    interval: 60

    detection_modes:
      # Type for health checking, valid values include:
      # NODE_STATUS_POLLING, NODE_STATUS_POLL_URL, LIFECYCLE_EVENTS
      - type: NODE_STATUS_POLLING

  recovery:
    # Action that can be retried on a failed node, will improve to
    # support multiple actions in the future. Valid values include:
    # REBOOT, REBUILD, RECREATE
    actions:
      - name: RECREATE
```

2. Membuat policy
> openstack cluster policy create --spec-file health_policy.yml health_policy

3. Attach policy ke cluster
> openstack cluster policy attach --policy health_policy mycluster

4. Cek list policy yang di attach ke mycluster
> openstack cluster policy binding list mycluster

5. Cek list node di mycluster
> openstack cluster members list mycluster

6. Matikan salah satu server yang memiliki ID sama dgn node di cluster :
> openstack server stop *id_members*

7. Cek list node di mycluster
> watch openstack cluster members list mycluster